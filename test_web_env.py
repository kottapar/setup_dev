def test_httpd_is_installed(host):
    httpd = host.service("httpd")

    assert httpd.is_running


def test_dev3_can_write_to_dev3(host):
    cmd = host.run("su - dev3 -c 'touch /dev_workspace/dev3/abc'")
    assert cmd.rc == 0


def test_dev3_can_read_from_dev3(host):
    cmd = host.run("su - dev3 -c 'cat /dev_workspace/dev3/abc'")
    assert cmd.rc == 0


def test_dev3_cant_write_to_dev23(host):
    cmd = host.run("su - dev23 -c 'touch /dev_workspace/dev3/abc'")
    assert cmd.rc == 1
