# Ansible playbook to setup dev env
-----------------------------------

The Ansible playbook `setup_dev_webserver.yml` will install Apache on the target server using a dependent role and will configure directories for developers to host their code.
It will configure Apache so that the devs will be assigned a directory and a non-standard port using which the dev can access the content.

VM Pre-requisites
-----------------
We would require a VM with Ansible installed to get started. Run `git clone https://kottapar@bitbucket.org/kottapar/setup_dev.git` to get started.

This is created and tested on a `Ubuntu 18.04` VM with Ansible 2.8.3.

```
root@ansiblevm:~/node-mongo-crud# lsb_release -a
No LSB modules are available.
Distributor ID: Ubuntu
Description:    Ubuntu 18.04.3 LTS
Release:        18.04
Codename:       bionic

root@ansiblevm:~/setup_dev# ansible --version
ansible 2.8.3
```

The target server can be any Centos/Redhat or Ubuntu VM which is accessible from the Ansible VM. We need to ensure that the target VMs are accessible via passwordless SSH for Ansible to be able to connect and perform the configuration changes. 

For example if we're adding `192.168.65.101` use ssh-copy-id to copy the SSH key to the target server.

```
root@ansiblevm:~/ravi/setup_dev# ssh-copy-id 192.168.65.101
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/root/.ssh/id_rsa.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
root@192.168.65.101's password:

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh '192.168.65.101'"
and check to make sure that only the key(s) you wanted were added.
```

Example for setting up the development environment
--------------------------------------------------
An example is provided below. For simplicity we're using root as the Ansible user. `ansiblevm` is the VM on which Ansible is installed. The below example will configure a VM `webserver02`

```
root@ansiblevm:~/ravi# git clone https://kottapar@bitbucket.org/kottapar/setup_dev.git
Cloning into 'setup_dev'...
remote: Counting objects: 75, done.
remote: Compressing objects: 100% (52/52), done.
remote: Total 75 (delta 16), reused 0 (delta 0)
Unpacking objects: 100% (75/75), done.
root@ansiblevm:~/ravi# cd setup_dev/
 
root@ansiblevm:~/ravi/setup_dev# cat hosts
[webservers]
192.168.65.201
```

Edit the `hosts` file and replace the ip address with the target VM or VMs which is/are to be configured. After the change:

```
root@ansiblevm:~/ravi/setup_dev# cat hosts
[webservers]
192.168.65.202
```

Playbook pre-requisites
-----------------------
To ensure that the target server has Apache installed, we'll pull a production config role to apply on the target server. 

Execute `ansible-galaxy install --force -r requirements.yml` to update the apache role. This fetches the latest `apache` role from the repo.

For now It is already present in this repo and is latest. So we'll continue.

Running the playbook
--------------------

Execute `ansible-playbook setup_dev_webserver.yml`

* This playbook prompts for a developer userid and then configures the target host.
    * It runs the dependent apache role
    * sets up a directory for the user and configures httpd for the user
    * changes the SELinux context for the user's directories so that httpd can read/write to them
    * adds the user's port to the list of SELinux authorized ports
	
We'll configure the directories and ports for the users `dev23` and `dev3`. We'll use these users to verify the current setup.

```
root@ansiblevm:~/ravi/setup_dev# ansible-playbook setup_dev_webserver.yml
Enter the developer userid: dev23

PLAY [setup the environment for developers on the web server] ************************************************************************

TASK [Gathering Facts] ***************************************************************************************************************
ok: [192.168.65.202]

TASK [apache : Install Apache] *******************************************************************************************************
ok: [192.168.65.202]

** snipped **

RUNNING HANDLER [dev_setup_role : restart httpd] *************************************************************************************
changed: [192.168.65.202]

PLAY RECAP ***************************************************************************************************************************
192.168.65.202             : ok=17   changed=9    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

Similarly run the playbook again for another user dev3

# Test the Ansible playbook changes
-----------------------------------

We are using Testinfra to verify the configuration changes made using Ansible previously. We'll create a python virtual environment to install testinfra.

Pre-requisites
--------------

Run the below commands to create a Python virtual environment and install testinfra.

`virtualenv testinfra`

`source testinfra/bin/activate`

`pip install testinfra`

Ensure that the users dev23 and dev3 are created. The script uses them to perform the tests. If you want to test other users that you've created you can edit the `test_web_env.py` and replace dev23 and dev3 with your users.

Execute `pytest -v --connection=ansible test_web_env.py` to run the test. The `--connection=ansible` ensures that the test is performed on the Ansible target host in the `hosts` file. 

Example
-------

```
root@ansiblevm:~/ravi/setup_dev# virtualenv testinfra
Using base prefix '/usr'
New python executable in /root/ravi/setup_dev/testinfra/bin/python3
Also creating executable in /root/ravi/setup_dev/testinfra/bin/python
Installing setuptools, pip, wheel...
done.
root@ansiblevm:~/ravi/setup_dev# source testinfra/bin/activate
(testinfra) root@ansiblevm:~/ravi/setup_dev# pip install testinfra
Collecting testinfra
**snipped**
Successfully installed
```

Run the test
------------

```
(testinfra) root@ansiblevm:~/ravi/setup_dev# pytest -v --connection=ansible test_web_env.py
======================================================== test session starts =========================================================
platform linux -- Python 3.6.8, pytest-5.1.2, py-1.8.0, pluggy-0.12.0 -- /root/ravi/setup_dev/testinfra/bin/python3
cachedir: .pytest_cache
rootdir: /root/ravi/setup_dev
plugins: testinfra-3.2.0
collected 4 items

test_web_env.py::test_httpd_is_installed[ansible://192.168.65.202] PASSED                                                      [ 25%]
test_web_env.py::test_dev3_can_write_to_dev3[ansible://192.168.65.202] PASSED                                                  [ 50%]
test_web_env.py::test_dev3_can_read_from_dev3[ansible://192.168.65.202] PASSED                                                 [ 75%]
test_web_env.py::test_dev3_cant_write_to_dev23[ansible://192.168.65.202] PASSED                                                [100%]

========================================================= 4 passed in 5.85s ==========================================================
(testinfra) root@ansiblevm:~/ravi/setup_dev#

```
